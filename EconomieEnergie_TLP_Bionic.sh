#!/bin/bash
# Script crée par Simbd

# Vérification que le script est lancé avec les bons droits
if [ "$UID" -ne "0" ]
then
  echo "Merci de lancer le script avec les droits admin ==> sudo ./script"
  exit 
fi 

# Description : TLP pour économie d'énergie pour les pc portable.

# Installation TLP
apt install tlp tlp-rdw -y
systemctl enable tlp
systemctl enable tlp-sleep

# Pour désactiver Bluethooth au démarrage :
#(idéalement à placer juste après : # Radio devices to disable on startup: bluetooth wifi wwan)
echo 'DEVICES_TO_DISABLE_ON_STARTUP="bluetooth"' >> /etc/default/tlp 
# + éventuellement : sudo update-rc.d -f bluetooth remove

# Pour lire les info qui passe concernant TLP : sudo tlp-stat
# Pour être sûr qu'il est activé : sudo tlp-stat | grep "TLP"
# Doc TLP : http://linrunner.de/en/tlp/docs/tlp-configuration.html

## Ajout de Tlp-UI, une interface en python permettant de gérer facilement toute la conf de TLP

# Récupération archive + icone
wget https://github.com/d4nj1/TLPUI/archive/master.zip ; unzip master.zip ; rm master.zip ; mv TLPUI-master /opt/
wget http://nux87.free.fr/script-postinstall-ubuntu/theme/tlpui.png ; mv tlpui.png /usr/share/icons/

# Pour que le futur lanceur fonctionne correctement
echo "#!/bin/bash
cd /opt/TLPUI-master/
python3 -m tlpui" > /opt/TLPUI-master/start.sh
chmod +x /opt/TLPUI-master/start.sh

# Création d'un lanceur 
echo "[Desktop Entry]
Name=TtpUi
Comment=Permet de configurer TLP simplement via une interface graphique
Icon=/usr/share/icons/tlpui.png
Exec=/opt/TLPUI-master/start.sh
Terminal=false
Type=Application
Categories=Outils" > /usr/share/applications/tlpui.desktop


