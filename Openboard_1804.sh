#!/bin/bash
# Script crée par Simbd
# source pour le deb : https://github.com/OpenBoard-org/OpenBoard/issues/138

# Description : Installation d'OpenBoard pour la 18.04 (sinon pose problème)

if [ "$UID" -ne "0" ]
then
  echo "Merci de lancer le script avec le droit admin => sudo ./nomduscript.sh"
  exit 
fi 

wget http://nux87.free.fr/script-postinstall-ubuntu/deb/openboard_bionic_140_x64.deb
dpkg -i openboard_bionic_140_x64.deb ; rm openboard_bionic_140_x64.deb
apt install -fy

# autres dépendances manquantes
apt install libqt5webkit5 libqt5script5 libqt5multimediawidgets5 libqt5xml5 -y

# modification du contenu du lanceur (écrasement de l'ancien contenu) sinon il ne fonctionne pas
echo "[Desktop Entry]
Version=1.4.0
Encoding=UTF-8
Name=OpenBoard
Comment=tableau blanc interactif pour les écoles et les universités
Exec=/opt/openboard/OpenBoard 
Icon=/opt/openboard/OpenBoard.png
StartupNotify=true
Terminal=false
Type=Application
Categories=Education;" > /usr/share/applications/openboard.desktop
