#!/bin/bash
# Script crée par Simbd

# Description : Ce script sert à automatiser l'installation du logiciel d'astronomie "Celestia" pour Ubuntu 18.04 Bionic Beaver X64

# Testé/Validé pour Ubuntu 18.04LTS Bionic Beaver X64 avec Gnome Shell 

if [ "$UID" -ne "0" ]
then
  echo "Merci de lancer le script avec le droit admin => sudo ./nomduscript.sh"
  exit 
fi 

# Récupération dépendance manquante 
wget http://nux87.free.fr/script-postinstall-ubuntu/deb/libpng12amd64.deb
dpkg -i libpng12*

# Dépendances nécessaires
apt install liblua5.1-0 freeglut3 libgtkglext1 libgnome2-0 libgnomeui-0 -y

#Récup célestia
wget http://nux87.free.fr/script-postinstall-ubuntu/deb/celestia-common1.6.1all.deb
wget http://fr.archive.ubuntu.com/ubuntu/pool/multiverse/c/celestia-nonfree/celestia-common-nonfree_1.6.1-1_all.deb #pour avoir les textures des objets stellaires 
wget http://nux87.free.fr/script-postinstall-ubuntu/deb/celestia-gnome1.6.1amd64.deb # <= Pour Gnome ou Cinnamon ou Mate !
# NB : Si vous utilisez l'environnement Kde/Plasma (par ex Kubuntu), il faudra récupérer le paquet "celestia-kde" (non testé)

dpkg -i celestia*
apt install -fy

#nettoyage
rm libpng12*.deb celestia*
clear
